const {Router} = require('express');
const userController = require('../controllers/user.controller');
const loginController = require('../controllers/login.controller');

const router = new Router();

router.use('/user', userController)
router.use('/login', loginController)

module.exports = router