const express = require('express')
const UserService = require('../services/user.service')
const router = express.Router()
const userService = new UserService()
const bcrypt = require('bcrypt')
const jwt = require('jwt-simple')
require('dotenv').config()


router.post('/singin', async(req, resp) => {
    
    userService.findUserByEmail(req.body.email)
    .then( async (user)=>{

      try {
        if( await bcrypt.compareSync(req.body.password, user.password)){

            resp.status(200).json({ 
                status: 'success', 
                message: 'Login successful',
                user: user,
                token: jwt.encode({userId: user._id}, process.env.KEY_SECRET)
            }) 

        }else{
            resp.status(403).json({
                status: 'fail',
                message: 'Please, verify the user or password'
            })
        }
      } catch (error) {
          console.log(error);
          resp.status(500).json({message: 'fail'})
      }
        
    })
    .catch((err) => {
        console.log(err)
        resp.status(503).json({message: 'faisl'})
    })
        
})


module.exports = router