const express = require('express');
const UserService = require('../services/user.service');
const router = express.Router();
const userService = new UserService();

router.post('/save', async(req, resp) => {
    
    userService.addUser(req.body)
    .then((id)=>{
        resp.status(200).json({message: 'success', id})
    })
    .catch(() => {
        resp.status(503).json({message: 'failure'})
    })
        
})

router.get('/all', async (req, resp) => {

    userService.getAllUsers()
        .then(users => {
            resp.status(200).json({ users })
        })
        .catch(() => {
            resp.status(503).json({message: 'failure'})
        })
})

router.get('/delete/:id', async (req, resp) => {
    
    userService.deleteUserById(req.params.id)
        .then(()=>{
            resp.status(200).json({message: 'success'})
        })
        .catch((message) => {
            resp.status(503).json({message})
        })
})

router.put('/update/:id', async (req, resp) => {

   
    userService.updateUserById(req.params.id, req.body)  
        .then(() => {
            resp.status(200).json({message: 'success'})
        })
        .catch(() => {
            resp.status(200).json({message: 'failure'})
        })

})

module.exports = router



