const mongoose = require('mongoose')
const timestamp = require('mongoose-timestamp')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')

const User = new Schema({

   username: String,
   password: String,
   email: String,
   firstName: String,
   lastName: String,
   phoneNumber: Number,
   rol: String,
   lastSession:[Date],
   enterprise: String

})

User.plugin(timestamp);
User.pre('save', function(){
   this.password = bcrypt.hashSync(this.password, 10);
})
module.exports = mongoose.model('User', User)
