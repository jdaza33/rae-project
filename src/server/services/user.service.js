const User = require('../models/user')

module.exports = class UserService {

    async addUser(user){
        
        return new Promise((resolve, reject)=>{
            User.create(user)    
            .then(res => {
                resolve(res._id)
            })
            .catch(error => {
                reject(error)
            })
        })
    }

    async getAllUser() {

        return new Promise((resolve, reject)=>{
            User.find()
            .then(res => {
                resolve(res)
            })
            .catch(error => {
                reject(error)
            })
        })
    } 

    async deleteUserById(id) { 
     
        return new Promise((resolve, reject)=>{
            User.deleteOne({_id: id})
            .then(res => {
                resolve(res)
            })
            .catch(error => {
                reject(error)
            })
        })
    }

    async updateUserById(id, updatedUser) {

        return new Promise((resolve, reject)=>{
            User.findByIdAndUpdate(id, updatedUser)
            .then(res => {
                resolve(res)
            })
            .catch(error => {
                reject(error)
            })
        })    
    }

    async findUserById(id) {

        return new Promise((resolve, reject) => {
            User.findById(id)
            .then(res => {
                resolve(res)
            })
            .catch(error => {
                reject(error)
            })
        })       
    }

    async findUserByEmail(email) {

        return new Promise((resolve, reject) => {
            User.findOne({ email: email })
            .then(res => {
                resolve(res)
            })
            .catch(error => {
                reject(error)
            })
        })       
    }
}