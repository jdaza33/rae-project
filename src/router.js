import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Reset from './components/ResetPassword.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: "/resetpassword",
      name: "reset",
      component: Reset
    }
  ]
});
